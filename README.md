(m,n,k) Game Solver
================================================================================

Solver for the [m,n,k game](http://en.wikipedia.org/wiki/M,n,k-game) instances 
with 0 < m,n < 100.

Solves instances of the (m,n,k) which is a generalization of Tic-Tac-Toe to
an m x n board where the first player to connect k cells horizontally, 
vertically or diagonally wins. 

This solver is stack-based since it is meant to be synthesized by high-level
synthesis to run on an FPGA. Solutions to arbitrarily large m,n would require
dynamic resizing of the stack; a solution using (stack-free) tail recursion
would probably be better suited in that case.

For a detailed analysis and rationale for the results see:
http://www.mathrec.org/old/2002jan/solutions.html

Compilation
--------------------------------------------------------------------------------
`make` based compilation, simply try 

	make

for optimized version or

	DEBUG=1 make

for version with (lots of!) debug output.

Usage
--------------------------------------------------------------------------------
Two executables should have been built, `mnk` is the basic solver for a single
instance, `mnk-tb` checks the implementation against several precomputed 
results. To solve Tic-Tac-Toe, i.e. the (3,3,3) game, run

	./mnk 3 3 3

Have fun!
