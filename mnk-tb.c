/** @brief Simple testbench for (m,n,k) game solver. Runs several instances 
 *         sequentially and compares against precomputed results. 
 **/
#include <stdio.h>
#include <stdlib.h>
#include "mnk.h"

/* testcase parameters */
struct mnk_params {
	ulong m;
	ulong n;
	ulong k;
	ullong games;
	ullong p1wins;
	ullong p2wins;
	ullong draws;
	ullong exp_games;
	ullong exp_p1wins;
	ullong exp_p2wins;
	ullong exp_draws;
};

/* bunch of precomputed testcases of increasing difficulty */
struct mnk_params tests[] = {
	{
		.m = 1, .n = 1, .k = 1, .exp_games = 1, .exp_p1wins = 1, 
		.exp_p2wins = 0, .exp_draws = 0 
	},
	{
		.m = 2, .n = 2, .k = 1, .exp_games = 4, .exp_p1wins = 4, 
		.exp_p2wins = 0, .exp_draws = 0 
	},
	{
		.m = 2, .n = 2, .k = 2, .exp_games = 24, .exp_p1wins = 24, 
		.exp_p2wins = 0, .exp_draws = 0 
	},
	{
		.m = 3, .n = 1, .k = 2, .exp_games = 6,
		.exp_p1wins = 4, .exp_p2wins = 0, .exp_draws = 2
	},
	{
		.m = 1, .n = 3, .k = 2, .exp_games = 6,
		.exp_p1wins = 4, .exp_p2wins = 0, .exp_draws = 2
	},
	{
		.m = 4, .n = 2, .k = 2, .exp_games = 1824,
		.exp_p1wins = 1408, .exp_p2wins = 416, .exp_draws = 0
	},
	{
		.m = 2, .n = 4, .k = 2, .exp_games = 1824,
		.exp_p1wins = 1408, .exp_p2wins = 416, .exp_draws = 0
	},
	{
		.m = 3, .n = 3, .k = 3, .exp_games = 255168, 
		.exp_p1wins = 131184, .exp_p2wins = 77904, .exp_draws = 46080
	},
	{
		.m = 4, .n = 3, .k = 3, .exp_games = 151188768, 
		.exp_p1wins = 79797600, .exp_p2wins = 56875968, .exp_draws = 14515200
	},
	{
		.m = 3, .n = 4, .k = 3, .exp_games = 151188768, 
		.exp_p1wins = 79797600, .exp_p2wins = 56875968, .exp_draws = 14515200
	},
};

/** @brief Runs the test at testcases[idx] and returns the number of errors.
 * @param  idx index into tests array
 * @return number of errors, 0 if all checks passed
 **/
int solve_test(const ulong idx)
{
	int err = 0;
	solve(tests[idx].m, tests[idx].n, tests[idx].k, &tests[idx].games,
			&tests[idx].p1wins, &tests[idx].p2wins, 
			&tests[idx].draws);
	printf("(%lu,%lu,%lu):\n", tests[idx].m, tests[idx].n, tests[idx].k);
	printf("\tgames: %llu, expected %llu (%s)\n",
			tests[idx].games, tests[idx].exp_games,
			tests[idx].games == tests[idx].exp_games ? 
			"PASS" : "FAIL");
	if (tests[idx].games != tests[idx].exp_games) ++err;
	printf("\tp1wins: %llu, expected %llu; p1 wins %3.2f%% (%s)\n",
			tests[idx].p1wins, tests[idx].exp_p1wins,
			(double)tests[idx].p1wins  * 100.0 / (double)tests[idx].games,
			tests[idx].p1wins == tests[idx].exp_p1wins ? 
			"PASS" : "FAIL");
	if (tests[idx].p1wins != tests[idx].exp_p1wins) ++err;
	printf("\tp2wins: %llu, expected %llu; p2 wins %3.2f%% (%s)\n",
			tests[idx].p2wins, tests[idx].exp_p2wins,
			(double)tests[idx].p2wins  * 100.0 / (double)tests[idx].games,
			tests[idx].p2wins == tests[idx].exp_p2wins ? 
			"PASS" : "FAIL");
	if (tests[idx].p2wins != tests[idx].exp_p2wins) ++err;
	printf("\tdraws: %llu, expected %llu; draws are %3.2f%% (%s)\n",
			tests[idx].draws, tests[idx].exp_draws,
			(double)tests[idx].draws  * 100.0 / (double)tests[idx].games,
			tests[idx].draws == tests[idx].exp_draws ? 
			"PASS" : "FAIL");
	if (tests[idx].draws != tests[idx].exp_draws) ++err;
	return err;
}

int main(int argc, char **argv)
{
	int errs;
	unsigned long i;
	(void)argc; (void)argv;
	/*
	ulong thrdcnt;
	if (argc < 2 || (thrdcnt = strtoul(argv[1], NULL, 0)) == 0) 
		fprintf(stderr, "Usage: mnk-tb <THREAD COUNT>\n"), exit(1);
	*/

	for (i = 0, errs = 0; i < sizeof(tests) / sizeof(*tests); ++i)
		errs += solve_test(i);
	printf("***********************************************************\n");
	if (errs)
		printf("TESTS FAILED: %d\n", errs);
	else
		printf("ALL TESTS PASSED!\n");

	return errs;
}

