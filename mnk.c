/** @file mnk.c
 * @brief Solver for the m,n,k game with 0 < m,n < 100.
 *
 * Solves instances of the (m,n,k) which is a generalization of Tic-Tac-Toe to
 * an m x n board where the first player to connect k cells horizontally, 
 * vertically or diagonally wins. For a more detailed description of the game
 * see e.g. http://en.wikipedia.org/wiki/M,n,k-game
 *
 * This solver is stack-based since it is meant to be synthesized by high-level
 * synthesis to run on an FPGA. Solutions to arbitrarily large m,n would require
 * dynamic resizing of the stack; a solution using (stack-free) tail recursion
 * would probably be better suited in that case.
 *
 * For a detailed analysis and rationale for the results see:
 * http://www.mathrec.org/old/2002jan/solutions.html
 *
 * @author Jens Korinth, jk@esa.cs.tu-darmstadt.de
 * @date 2014/05/15
 **/
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>

#ifndef NTHREADS
#include <pthread.h>
#endif

#include "mnk.h"

#ifndef NDEBUG
	#ifndef NTHREADS
	#define DBG(fmt, ...) fprintf(stderr, "|%lu|%s: " fmt, pthread_self(), \
			__func__, ##__VA_ARGS__)
	#else
	#define DBG(fmt, ...) fprintf(stderr, "%s: " fmt, __func__, \
			##__VA_ARGS__)
	#endif
#else
	#define DBG(fmt, ...)
#endif

#define MAX_M			100UL
#define MAX_N			100UL

typedef unsigned char cell_t;
typedef unsigned short move_t;

enum cell_status_t {
	EMPTY,
	P1,
	P2,
};

/* state of solver: includes grid etc. */
struct solver_state {
	/* main params */
	ulong m, n, k;

	/* grid */
	cell_t grid[MAX_N][MAX_M]; // = { { 0 } };

	/* stack pointer */
	ulong ss; // = 0;

	/* move stack */
	move_t stack[MAX_M * MAX_N]; // = { 0 };

	/* for each move: first allowed index (prevent loops) */
	ulong at[MAX_M * MAX_N]; // = { 0 };

	/* backtrack limit */
	ulong btlimit;
};

#ifndef NTHREADS
/* thread start routine parameter */
struct thread_param {
	ulong m, n, k, thrdnr;
	ullong *games, *p1wins, *p2wins, *draws;
};
#endif

/******************************************************************************/
#ifndef NDEBUG
static
void print_grid(cell_t grid[MAX_N][MAX_M], const ulong m, const ulong n)
{
	ulong row, col;
	for (row = 0; row < n; ++row) {
#ifndef NTHREADS
		fprintf(stderr, "|%lu| ", pthread_self());
#endif
		for (col = 0; col < m; ++col) {
			fprintf(stderr, "%c", grid[row][col] == P1 ? 'X' : 
					grid[row][col] == P2 ? 'O' : '.');
		}
		fprintf(stderr, "\n");
	}
}

static
void print_stack(const move_t stack[MAX_N*MAX_M], const ulong m, const ulong n)
{
	ulong row, col, idx;
	/*
	for (idx = 0; idx < m * n; ++idx) 
		printf("stack[%lu] = (%lu, %lu)\n", idx, stack[idx] / m, 
				stack[idx] % m);
	*/

	for (row = 0; row < n; ++row) {
#ifndef NTHREADS
		fprintf(stderr, "|%lu| ", pthread_self());
#endif
		for (col = 0; col < m; ++col) {
			idx = 0;
			while (idx < n * m && stack[idx] != row * m + col)
				++idx;
			// assert(idx < n * m);
			fprintf(stderr, "%lu", idx < n * m ? idx + 1 : 0);
		}
		fprintf(stderr, "\n");
	}
}
#endif

/** @brief Performs a valid move on n x m grid, if possible.
 * @param  s pointer to solver state
 * @return true, if valid move was possible, false otherwise  
 **/
int select_move(struct solver_state *s)
{
	assert(s);
	const ulong d = s->m * s->n;
	ulong i;
	/* all moves in game taken? */
	if (s->ss >= d) 
		return 0;
	DBG("searching for move beginning at (%lu, %lu)\n", 
			s->at[s->ss] / s->m, s->at[s->ss] % s->m);
	i = s->at[s->ss];
	/* TODO change to single index to remove div and mod */
	while (i < d && s->grid[i / s->m][i % s->m] != EMPTY) 
		++i;
	/* no more moves at this point? */
	if (i >= d)
		return 0;
	/* a valid move was found */
	DBG("found valid move at (%lu, %lu)\n", i / s->m, i % s->m);
	s->grid[i / s->m][i % s->m] = s->ss % 2 > 0 ? P2 : P1;
	s->at[s->ss] = i;
	s->stack[s->ss++] = i;
	return 1;
}

/** @brief Undoes the last move.
 * @param  s pointer to solver state
 * @return true, if undoing was possible, false on empty stack / btlimit reached
 **/
int backtrack(struct solver_state *s)
{
	assert(s);
	/* no more backtracking possible? */
	if (s->ss == s->btlimit)
		return 0;
	const move_t mv = s->stack[--s->ss];
	DBG("undoing move #%lu (%lu, %lu)\n", s->ss, mv / s->m, mv % s->m);
	s->grid[mv / s->m][mv % s->m] = EMPTY;
	s->stack[s->ss] = 0;
	s->at[s->ss] = mv + 1;
	if (s->ss + 1 < s->m * s->n) s->at[s->ss+1] = 0;
	return 1;
}

/** @brief Checks if a player has won in the current (m,n,k) game.
 *
 * If stack size is less than k*2-1 it is impossible for any player to have won.
 * Otherwise iterates once over every cell, if non-empty:
 *  1. checks if k-1 fields to the right belong to same player => win
 *  2. checks if k-1 fields below belong to same player => win
 *  3. checks if k-1 fields in below right diagonal belong to same player => win
 *  4. checks if k-1 fields in below left diagonal belong to same player => win
 * Search can be aborted as soon as any of these conditions is met.
 *
 * @remark I'm sure this can be optimized (e.g. skipping more fields in the main
 * 	   loops, if no k-seq can be fitted in anymore), but I don't have the 
 * 	   time to investigate at the moment.
 * @param  s pointer to solver state
 * @return P1 if player 1 has won, P2 if player 2 has won, EMPTY otherwise
 **/
enum
cell_status_t game_result(struct solver_state *s)
{
	assert(s);
	ulong i, j;
	if (s->ss < (s->k << 1) - 1) return EMPTY; /* winning is not possible yet */
	for (j = 0; j < s->n; ++j) {
		for (i = 0; i < s->m; ++i) {
			const enum cell_status_t p = s->grid[j][i];
			/* empty fields cannot start winning condition */
			if (p == EMPTY) continue;
			/* start check at (i, j): horizontal */
			if (i <= s->m - s->k) { /* only check if rest of row >= k */
				ulong c = i + 1;
				ulong l = 1;
				while (s->grid[j][c++] == p && ++l < s->k);
				/* winning row found, abort search */
				if (l >= s->k)
					return p;
			}
			/* vertical */
			if (j <= s->n - s->k) { /* only check if rest of col >= k */
				ulong c = j + 1;
				ulong l = 1;
				while (s->grid[c++][i] == p && ++l < s->k);
				/* winning col found, abort search */
				if (l >= s->k)
					return p;
			}
			/* pos diagonal */
			if (j <= s->n - s->k && i <= s->m - s->k) { /* only check if both */
				ulong c = j + 1;
				ulong d = i + 1;
				ulong l = 1;
				while (s->grid[c++][d++] == p && ++l < s->k);
				/* winning diag found, abort search */
				if (l >= s->k)
					return p;
			}
			/* neg diagonal */
			if (j <= s->n - s->k && i >= s->k-1) { /* only check if both */
				ulong c = j + 1;
				ulong d = i - 1;
				ulong l = 1;
				while (s->grid[c++][d--] == p && ++l < s->k);
				/* winning diag found, abort search */
				if (l >= s->k)
					return p;
			}
		}
	}
	return EMPTY;
}


/******************************************************************************/
/** @brief Main solver routine: Computes the total number of games, wins for 
 *         each player and draws for given (m,n,k) game.
 *
 * Main loop is simple: 
 *  1. check if a player has won, or the stack is full (all cells used);
 *     if that is the case, count win/draw accordingly and undo last move in 
 *     case the board is not full; then continue, unless backtracking was not
 *     possible
 *  2. try to make a move; if not possible, backtrack by undoing last move, if
 *     backtracking is not possible, stop (search space exhausted), else cont.
 *  3. repeat 
 * @param  m width of grid
 * @param  n height of grid
 * @param  k size of winning seq
 * @param  thrdid if > 0, thrdid wil be used as first move and backtracking
 *         will be limited to 1, i.e. up to the first move 
 * @param  games output: total number of games (mandatory)
 * @param  p1wins output: total number of player 1 wins (mandatory)
 * @param  p2wins output: total number of player 2 wins (mandatory)
 * @param  draws output: total number of draws (mandatory)
 **/
void solve_threaded(const ulong m, const ulong n, const ulong k, const ulong thrdid,
		ullong *games, ullong *p1wins, ullong *p2wins, ullong *draws)
{
	assert(games);
	assert(p1wins);
	assert(p2wins);
	assert(draws);
	assert(thrdid <= m * n);

	/* reset counters */
	*games = *p1wins = *p2wins = *draws = 0;
	/* make solver state from params */
	struct solver_state s;
	s.m = m;
	s.n = n;
	s.k = k;
	memset(s.grid, 0, sizeof(s.grid));
	memset(s.stack, 0, sizeof(s.stack));
	memset(s.at, 0, sizeof(s.at));
	s.ss = 0;
	s.btlimit = 0;

	/* if thrdid > 0, place as first move, and limit backtracking */
	if (thrdid) {
		move_t mv = thrdid - 1;	
		DBG("thrdid == %lu\n", thrdid);
		s.stack[s.ss++] = mv; 		/* push first move to stack */
		s.grid[mv / s.m][mv % s.m] = P1;
		s.at[0] = s.n * s.m; 		/* prevent backtracking */
		s.btlimit = 1;
	}

	/* number of moves per game, stack depth */
	const ulong d = m * n;
	while (1) {
		/* has somebody won? */
		const enum cell_status_t result = game_result(&s);
		/* is the game finished (board full/won)? */
		if (s.ss == d || result != EMPTY) {
#ifndef NDEBUG
			DBG("Solution #%llu:\n", *games);
			print_grid(s.grid, m, n); 
			print_stack(s.stack, m, n); 
#endif
			++(*games);
			switch (result) {
			case P1: ++(*p1wins); break;
			case P2: ++(*p2wins); break;
			default: ++(*draws); break;
			}
			DBG("result is %s\n", result == EMPTY ? "a draw" :
					result == P1 ? "player 1 wins" :
					"player 2 wins");
			/* undo winning move, in case board is not full */
			if (s.ss != d) {
				if (! backtrack(&s)) {
					DBG("No more moves to undo! ss = %lu\n", s.ss);
					break;
				}
			}
		}
		/* make a move */
		if (! select_move(&s)) {
			if (! backtrack(&s)) {
				DBG("No more moves to undo! ss = %lu\n", s.ss);
				break;
			}
		}
	}
}

#ifndef NTHREADS
/** @brief Wrapper routine for pthread start. **/
void *solve_thread(void *p)
{
	struct thread_param *tp = (struct thread_param *)p;
	solve_threaded(tp->m, tp->n, tp->k, tp->thrdnr, tp->games, tp->p1wins, 
			tp->p2wins, tp->draws);
	return NULL;
}
#endif

void solve(const ulong m, const ulong n, const ulong k,
		ullong *games, ullong *p1wins, ullong *p2wins, ullong *draws)
{
#ifdef NTHREADS
	solve_threaded(m, n, k, 0, games, p1wins, p2wins, draws);
#else
	const int thrdcnt = sysconf(_SC_NPROCESSORS_ONLN); /* detect cores */
	pthread_t thrd[thrdcnt];	/* array of pthreads */
	ullong t_games[thrdcnt];	/* game count for each thread */
	ullong t_p1wins[thrdcnt];	/* P1 wins for each thread */
	ullong t_p2wins[thrdcnt];	/* P2 wins for each thread */
	ullong t_draws[thrdcnt];	/* draws for each thread */

	struct thread_param t_p[thrdcnt]; /* internal thread parameters */

	int i;
	ulong j = 0;

	*games = *p1wins = *p2wins = *draws = 0;
	while (j < n * m) {
		for (i = 0; i < thrdcnt && j + i < m * n; ++i) {
			/* reset intermediate values */
			t_games[i] = t_p1wins[i] = t_p2wins[i] = t_draws[i] = 0;
			/* set thread params */
			t_p[i].m = m;
			t_p[i].n = n;
			t_p[i].k = k; 
			t_p[i].thrdnr = j + i + 1;
			t_p[i].games = &t_games[i];
			t_p[i].p1wins = &t_p1wins[i];
			t_p[i].p2wins = &t_p2wins[i];
			t_p[i].draws = &t_draws[i];

			/* start thread */
			int ret = pthread_create(&thrd[i], NULL, solve_thread, (void *)&t_p[i]);
			if (ret) {
				fprintf(stderr, "Could not create thread %u, return code %d\n", 
						i, ret);
				exit(1);
			}
		}
		/* wait for all to finish */
		for (i = 0; i < thrdcnt && j + i < m * n; ++i) {
			pthread_join(thrd[i], NULL);
			DBG("thread #%d returned: games = %llu, p1wins = %llu, p2wins = %llu, "
					"draws = %llu\n", i, t_games[i], t_p1wins[i], t_p2wins[i], 
					t_draws[i]);
			/* add up results */
			*games += t_games[i];
			*p1wins += t_p1wins[i];
			*p2wins += t_p2wins[i];
			*draws += t_draws[i];
		}
		DBG("starting new thread batch, j == %lu, thrdcnt == %u\n", j, thrdcnt);
		/* next batch */
		j += thrdcnt;
	}
#endif
}


/******************************************************************************/
/** @brief Prints help text and exits with code 1. **/
void print_usage_and_exit(void)
{
	fprintf(stderr, "Usage: mnk <M> <N> <K>\n"
			"\t<M>: 0 < field width < %lu\n"
			"\t<N>: 0 < field height < %lu\n"
			"\t<K>: length of winning connection > 0\n\n",
			MAX_N, MAX_M);
	exit(1);
}

/** @brief Tries to parse first three command line arguments as m, n and k 
 *         respectively; returns true on success, false on error. 
 * @param  argc argument count
 * @param  argv array of argument strings
 * @param  m output: width of grid (mandatory)
 * @param  n output: height of grid (mandatory)
 * @param  k output: size of winning seq (mandatory)
 * @return true, if parsing was successful (m,n,k valid), false otherwise
 **/
int parse_args(int argc, char **argv, ulong *m, ulong *n, ulong *k)
{
	assert(m); 
	assert(n); 
	assert(k);
	if (argc != 4) 
		return 0;
	*m = strtoul(argv[1], NULL, 0);
	if (*m == 0 || *m >= MAX_M)
		return 0;
	*n = strtoul(argv[2], NULL, 0);
	if (*n == 0 || *n >= MAX_N)
		return 0;
	*k = strtoul(argv[3], NULL, 0);
	if (*k == 0)
		return 0;
	return 1;
}

/* deactivate main for high-level synthesis */
#ifndef __HLS__
int main(int argc, char **argv)
{
	ulong m = 0, n = 0, k = 0;
	ullong games, p1wins, p2wins, draws;
	if (! parse_args(argc, argv, &m, &n, &k))
		print_usage_and_exit();
	printf("Solving the (%lu, %lu, %lu) game.\n", m, n, k);
	solve(m, n, k, &games, &p1wins, &p2wins, &draws);
	printf("Result of %llu games: p1 wins %llu games, p2 wins %llu games "
			"and %llu games are draws.\n", games, p1wins, p2wins, 
			draws);
	return 0;
}
#endif

