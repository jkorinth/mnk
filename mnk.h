#ifndef __MNK_H__
#define __MNK_H__

typedef unsigned char uchar;
typedef unsigned long ulong;
typedef unsigned long long ullong;

/** @brief Main solver routine: Computes the total number of games, wins for 
 *         each player and draws for given (m,n,k) game.
 *
 * Main loop is simple: 
 *  1. check if a player has won, or the stack is full (all cells used);
 *     if that is the case, count win/draw accordingly and undo last move in 
 *     case the board is not full; then continue, unless backtracking was not
 *     possible
 *  2. try to make a move; if not possible, backtrack by undoing last move, if
 *     backtracking is not possible, stop (search space exhausted), else cont.
 *  3. repeat 
 * @param  m width of grid
 * @param  n height of grid
 * @param  k size of winning seq
 * @param  games output: total number of games (mandatory)
 * @param  p1wins output: total number of player 1 wins (mandatory)
 * @param  p2wins output: total number of player 2 wins (mandatory)
 * @param  draws output: total number of draws (mandatory)
 **/
void solve(const ulong m, const ulong n, const ulong k,
		ullong *games, ullong *p1wins, ullong *p2wins, ullong *draws);

#endif /* __MNK_H__ */
