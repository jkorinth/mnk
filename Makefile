CC?=gcc
CCFLAGS=-g -Os -Wall -Werror -Wextra -pedantic -fPIC --std=gnu99 -Wno-gnu-zero-variadic-macro-arguments
LDFLAGS= 

ifdef DEBUG
	CCFLAGS+=
else
	CCFLAGS+=-O3 -DNDEBUG
endif

ifndef NTHREADS
	LDFLAGS+=-lpthread
else
	CCFLAGS+=-DNTHREADS
endif

all : mnk mnk-tb

%.o : %.c
	$(CROSS_COMPILE)$(CC) $(CCFLAGS) -c $^

mnk : mnk.o
	$(CROSS_COMPILE)$(CC) $(LDFLAGS) -o $@ $^

mnk-tb : mnk.c mnk-tb.c
	$(CROSS_COMPILE)$(CC) $(CCFLAGS) -o mnk-tb -D__HLS__ mnk.c mnk-tb.c $(LDFLAGS)

clean :
	@rm -rf mnk *.o mnk-tb.o mnk-tb

